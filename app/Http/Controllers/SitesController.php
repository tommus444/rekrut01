<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Activity;

class SitesController extends Controller
{
    public function index()
    {
    	return view('welcome');
    }
    public function add_client()
    {
    	return view('sites.add_client');
    }
    public function list_clients()
    {
    	$clients = Client::all();

		return view('sites.list_clients', compact('clients'));
    }
    public function show_client(Client $client)
    {
        return view('sites.show_client', compact('client'));
    }
    public function edit_client(Client $client)
    {
        return view('sites.edit_client', compact('client'));
    }

    public function add_client_activity(Client $client)
    {
        return view('sites.add_client_activity', compact('client'));
    }
    public function edit_client_activity(Activity $activity)
    {
        return view('sites.edit_client_activity', compact('activity'));
    }

    public function add_client_form(Request $request)
    {
    	$validateData = $request->validate([
    		'name' => 'required|min:5',
    		'country' => 'required',
    	]);

    	$client = new Client();
    	$client->name = $request->input('name');
    	$client->address = $request->input('address');
    	$client->country = $request->input('country');
    	$client->active = true;
    	$client->save();

        return redirect('list_clients');
    }
    public function update_client(Request $request){

        $validateData = $request->validate([
            'name' => 'required|min:5',
            'country' => 'required',
        ]);

        $client = Client::find($request->input('id'));
        $client->name = $request->input('name');
        $client->address = $request->input('address');
        $client->country = $request->input('country');
            if(is_null($request->input('active'))){
                $client->active = 0;
            }
            else{
                $client->active = $request->input('active');            
            }
        $client->save();

        return redirect('list_clients');
    }

    public function add_activity(Request $request){
        $activity = new Activity();        
        $activity->name = $request->input('name');
        $activity->date = $request->input('date');
        $activity->client_id = $request->input('client_id');
        $activity->description = $request->input('description');
        $activity->save();

        return redirect()->action('SitesController@show_client', ['id' =>$request->client_id]);
    }
     public function update_activity(Request $request){
        $activity_id = $request->input('activity_id');

        $activity = Activity::find($activity_id);
        $activity->name = $request->input('name');
        $activity->date = $request->input('date');
        $activity->description = $request->input('description');
        $activity->save();

        return redirect()->action('SitesController@show_client', ['id' =>$request->client_id]);
    }
    public function delete_activity($id){        
        $activity = Activity::find($id);
        $activity->delete();
        return redirect('list_clients');
    }
    public function delete_client($id){        
        $client = Client::find($id);
        $client->delete();
        return redirect('list_clients');
    }
}
