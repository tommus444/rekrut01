@extends('layouts.main')

@section('title') Card - {{$client->name}} @endsection

@section('content')
	<hr><h1>About - {{$client->name}}</h1><hr>
	<h3>{{$client->name}}</h3>
	<h3>{{$client->address}}</h3>
	<h3>{{$client->country}}</h3><hr>
	<h1>Activities list</h1><hr>
<?php 
	$activities = DB::table('activities')
		->where('client_id', $client->id)
		->orderBy('date', 'DESC')
		->get();

	if(!is_null($activities)){
	echo '<table class="table table-hover">
		<thead>
			<th>Name</th>
			<th>Description</th>
			<th>Date</th>	
			<th>Tools</th>	
		</thead>
		<tbody>';
		$question_delete = "'Are you sure you want to delete this item?'";
		foreach ($activities as $activity) {
			echo '<tr><td>'. $activity->name . '</td><td> ' . 
			$activity->description . '</td><td>'.$activity->date.'</td><td>
			<a href="'.route('sites.delete_activity', [$activity->id]).'" onclick="return confirm('.$question_delete.');">Delete</a> 
			<a href="'.route("sites.edit_client_activity", $activity->id).'">Edit</a></td></tr>';
		}
	echo '</tbody></table>';		
	}

?>
@endsection