@extends('layouts.main')
@section('title', 'Add client')


@section('content')
<h1>New client</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{route('sites.add_client_form')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<div class="form-group">
		<input type="text" name="name" placeholder="Name" class="form-control" required="true">
	</div>
	<div class="form-group">
		<textarea type="text" name="address" placeholder="Address" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<select name="country" class="form-control" id="" required="true">
			<option value="" disabled="true" selected="true">Select country</option>			
			<?php $countris = array("France", "Germany", "Italy", "Ukraine", "United Kingdom", "Poland", "Romania", "Russia", "Spain", "Turkey"); ?>
			@foreach($countris as $country)
			<option value="{{$country}}">{{$country}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
		<button class="btn btn-success float-right">Add client</button>
	</div>
</form>
@endsection