@extends('layouts.main')
@section('title', 'Clients list')

@section('content')
<h1>Clients list</h1>
	<table class="table table-hover">
		<thead>
			<th>Name</th>
			<th>Address</th>
			<th>Country</th>
			<th>Active</th>
			<th>Tools</th>
		</thead>
	@foreach($clients as $client)	
		<tr>
			<td>{{$client->name}}</td>
			<td>{{$client->address}}</td>
			<td>{{$client->country}}</td>
			<td>{{$client->active}}</td>
			<td>
				<a href="{{route('sites.show_client', $client)}}">SHOW</a> /
				<a href="{{route('sites.edit_client', $client)}}">EDIT</a> / 
				<a href="{{route('sites.add_client_activity', $client)}}">ADD ACTV</a> /
				<a href="{{route('sites.delete_client', $client->id)}}" onclick="return confirm('Are you sure you want to delete this item?');">DELETE</a>				
			</td>			
		</tr>
	@endforeach
	</table>

@endsection