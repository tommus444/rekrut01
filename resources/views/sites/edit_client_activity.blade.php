@extends('layouts.main')
@section('title') Edit activity @endsection


@section('content')
<h1>Edit activity </h1><hr>

<?php 	
	$activityData = DB::table('activities')->where('id', $activity->id)->first();

?> 
<form action="{{route('sites.update_activity')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<div class="form-group">
		<input type="hidden" name="activity_id" placeholder="activity_id" value="{{$activity->id}}" class="form-control" required="true">
		<input type="hidden" name="client_id" placeholder="client_id" value="{{$activity->client_id}}" class="form-control" required="true">
		<input type="text" name="name" placeholder="Name" value="{{$activityData->name}}" class="form-control" required="true">
	</div>
	<div class="form-group">
		<input type="date" name="date" value="{{$activityData->date}}" class="form-control" required="true">
	</div>
	<div class="form-group">
		<textarea type="text" name="description" placeholder="Description" class="form-control">{{$activityData->description}}</textarea>
	</div>

	<div class="form-group">
		<button class="btn btn-success float-right">Edit activity</button>
	</div>
</form>
@endsection