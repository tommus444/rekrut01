@extends('layouts.main')
@section('title') Edit - {{$client->name}} @endsection


@section('content')
<h1>Edit client - {{$client->name}}</h1><hr>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{route('sites.update_client')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<input type="hidden" name="id" value="{{$client->id}}">

	<div class="form-group">
		<input type="text" value="{{$client->name}}" name="name" placeholder="Name" class="form-control" required="true">
	</div>
	<div class="form-group">
		<textarea type="text" name="address" placeholder="Address" class="form-control">{{$client->address}}</textarea>
	</div>
	<div class="form-group">
		<select name="country" class="form-control" id="" required="true">			
			<?php $countris = array("France", "Germany", "Italy", "Ukraine", "United Kingdom", "Poland", "Romania", "Russia", "Spain", "Turkey"); ?>
			@foreach($countris as $country)
			<option value="{{$country}}" 
				<?php if ($country == $client->country){
					echo " selected ";				
				}
				?>
			>{{$country}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
		<span>Active: </span><input type="checkbox" name="active" value="1" <?php if($client->active == 1){echo "checked";} ?>>
	</div>
	<div class="form-group">
		<button class="btn btn-success float-right">Save changes</button>
	</div>
</form>
@endsection