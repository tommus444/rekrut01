@extends('layouts.main')
@section('title') Add activity - {{$client->name}} @endsection


@section('content')
<h1>Add activity to {{$client->name}}</h1><hr>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{route('sites.add_activity')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<div class="form-group">
		<input type="hidden" name="client_id" placeholder="client_id" value="{{$client->id}}" class="form-control" required="true">
		<input type="text" name="name" placeholder="Name" class="form-control" required="true">
	</div>
	<div class="form-group">
		<input type="date" name="date" value="<?php echo date("Y-m-d"); ?>" class="form-control" required="true">
	</div>
	<div class="form-group">
		<textarea type="text" name="description" placeholder="Description" class="form-control"></textarea>
	</div>

	<div class="form-group">
		<button class="btn btn-success float-right">Add activity</button>
	</div>
</form>
@endsection