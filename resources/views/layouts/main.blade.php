<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

	<div class="container">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="navbar-brand" href="{{action('SitesController@list_clients')}}">Clients list</a>
      </li>
      <li class="nav-item">
        <a class="navbar-brand" href="{{action('SitesController@add_client')}}">New client</a>
      </li>
    </ul>
  </div>
</nav><br>
	@yield('content')
	</div>
</body>
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/scripts.js') }}"></script>
</html>