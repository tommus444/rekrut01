<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SitesController@index');
Route::get('/list_clients', 'SitesController@list_clients');
Route::get('/add_client', 'SitesController@add_client');
Route::get('/show_client/{client}', [
	'uses' => 'SitesController@show_client',
	'as' => 'sites.show_client'
]);
Route::get('/edit_client/{client}', [
	'uses' => 'SitesController@edit_client',
	'as' => 'sites.edit_client'
]);
Route::get('/add_client_activity/{client}', [
	'uses' => 'SitesController@add_client_activity',
	'as' => 'sites.add_client_activity'
]);
Route::get('/edit_client_activity/{activity}', [
	'uses' => 'SitesController@edit_client_activity',
	'as' => 'sites.edit_client_activity'
]);

Route::post('/add_client_form', [
	'uses' => 'SitesController@add_client_form', 
	'as' => 'sites.add_client_form'
]);
Route::post('/update_client', [
	'uses' => 'SitesController@update_client',
	'as' => 'sites.update_client'
]);
Route::post('/update_activity', [
	'uses' => 'SitesController@update_activity',
	'as' => 'sites.update_activity'
]);
Route::post('/add_activity', [
	'uses' => 'SitesController@add_activity',
	'as' => 'sites.add_activity'
]);
Route::get('/delete_activity/{activity}', [
	'uses' => 'SitesController@delete_activity',
	'as' => 'sites.delete_activity'
]);
Route::get('/delete_client/{client}', [
	'uses' => 'SitesController@delete_client',
	'as' => 'sites.delete_client'
]);
Auth::routes();